#ifndef H_KMER_READWRITE
#define H_KMER_READWRITE

#include <iostream>

std::vector<kmer_entry> read_entries(std::istream& in, const int& k);

std::vector<kmer_entry> read_entries(std::istream& in, const int& k, const uint32_t& vec_size);

kmer_entry read_compressed_entry(std::istream& in, const int& k);

void write_compressed_entries(std::ostream& out, const std::vector<kmer_entry> & entries, const int&k);

void write_entries(std::ostream& out, const std::vector<kmer_entry>& entries, const int& k);

#endif
