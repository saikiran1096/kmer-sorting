#include <vector>

#include "consts.hpp"
#include "conversion.hpp"
#include "entries.hpp"
#include "readwrite.hpp"

std::vector<kmer_entry> read_entries(std::istream& in, const int& k){
  std::vector<kmer_entry> entries;
  std::string line;  
  
  while(in.peek()!=EOF){    
    std::vector<std::string> lines(READ_BATCH_SIZE);
    int n_found = 0;
    for(int i = 0; i < READ_BATCH_SIZE && in.peek() != EOF; ++i){
      getline(in, lines[i]);
      ++n_found;
    }

    std::vector<kmer_entry> batch(READ_BATCH_SIZE);
    
#pragma omp parallel for
    for(int i = 0; i < n_found; i++){
      std::string kmer = lines[i].substr(0, k);
      std::uint32_t count = std::stoi(lines[i].substr(k+1));
	
      batch[i] = {kmer_to_int(kmer, k), count};
    }

    for(int i = 0; i < n_found; i++){
      entries.push_back(batch[i]);
    }

  }

  return entries;
}

std::vector<kmer_entry> read_entries(std::istream& in, const int& k, const uint32_t& vec_size){
  std::vector<kmer_entry> entries;
  std::string line;  
  
  while(in.peek()!=EOF && entries.size() < vec_size){    
    std::vector<std::string> lines(READ_BATCH_SIZE);
    int n_found = 0;
    for(int i = 0; i < READ_BATCH_SIZE && in.peek() != EOF && entries.size() + n_found < vec_size; ++i){
      getline(in, lines[i]);
      ++n_found;
    }

    std::vector<kmer_entry> batch(READ_BATCH_SIZE);
    
#pragma omp parallel for
    for(int i = 0; i < n_found; i++){
      std::string kmer = lines[i].substr(0, k);
      std::uint32_t count = std::stoi(lines[i].substr(k+1));
	
      batch[i] = {kmer_to_int(kmer, k), count};
    }

    for(int i = 0; i < n_found; i++){
      entries.push_back(batch[i]);
    }

  }

  return entries;
}

kmer_entry read_compressed_entry(std::istream& in, const int& k){
  uint64_t kmer;
  in.read((char*) &kmer, sizeof(kmer));
  uint32_t count;
  in.read((char*) &count, sizeof(count));
  
  kmer_entry entry = {kmer, count};

  return entry;
}

void write_compressed_entries(std::ostream& out, const std::vector<kmer_entry> & entries, const int&k){
  for(size_t i = 0; i < entries.size(); i++){
    out.write((char*) &entries[i].int_kmer, sizeof(entries[i].int_kmer));
    out.write((char*) &entries[i].count, sizeof(entries[i].count));
  }
}

void write_entries(std::ostream& out, const std::vector<kmer_entry>& entries, const int& k){
  int n_lines = entries.size();
  int n_batches = n_lines / WRITE_BATCH_SIZE;
  if(n_lines % WRITE_BATCH_SIZE != 0){
    ++n_batches;
  }

  for(int i = 0; i < n_batches; ++i){
    int size = std::min(WRITE_BATCH_SIZE, n_lines - i*WRITE_BATCH_SIZE);
    std::vector<output_entry> batch(size);

#pragma omp parallel for
    for(int j = 0; j < size; j++){
      int index = i*WRITE_BATCH_SIZE + j;
      batch[j] = {int_to_kmer(entries[index].int_kmer, k), entries[index].count};
    }

    for(int j = 0; j < size; j++){
      out << batch[j].string_kmer << '\t' << batch[j].count << '\n';
    }
  } 
}
