.PHONY = all clean

CXX = g++
CC = g++
LDFLAGS = -fopenmp
CXXFLAGS = -Wall -fopenmp -std=c++17 -D_GLIBCXX_PARALLEL

SORTING_OBJS = kmer-sorting.o conversion.o readwrite.o

all: CXXFLAGS += -Ofast -march=native
all: LDFLAGS += -Ofast -march=native
all: kmer-sorting prog1

debug: CXXFLAGS += -Og -g -pg
debug: LDFLAGS += -Og -g -pg
debug: kmer-sorting prog1

kmer-sorting: $(SORTING_OBJS)

kmer-sorting.o: consts.hpp entries.hpp readwrite.hpp
conversion.o: conversion.hpp
readwrite.o: consts.hpp conversion.hpp entries.hpp readwrite.hpp

clean: 
	rm kmer-sorting prog1 $(SORTING_OBJS) 
