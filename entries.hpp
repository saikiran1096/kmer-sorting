#ifndef H_KMER_ENTRY
#define H_KMER_ENTRY

#include <cstdint>
#include <string>
#include <fstream>

struct kmer_entry {
  uint64_t int_kmer;
  uint32_t count;

  bool operator<(const kmer_entry& b) const{
    return int_kmer < b.int_kmer;
  }
} __attribute__((packed));


struct output_entry {
  std::string string_kmer;
  uint32_t count;
};

struct queue_entry {
  kmer_entry k_entry;
  size_t index;

  bool operator<(const queue_entry& b) const{
    return k_entry.int_kmer > b.k_entry.int_kmer;
  }
};

#endif
