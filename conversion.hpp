#ifndef H_KMER_CONVERSION
#define H_KMER_CONVERSION

#include <string>

uint64_t kmer_to_int(const std::string& kmer, const int& k);
std::string int_to_kmer(uint64_t kmer, const int& k);

#endif
