#ifndef H_KMER_CONSTS
#define H_KMER_CONSTS

constexpr int READ_BATCH_SIZE = 1000*1024;
constexpr int WRITE_BATCH_SIZE = 1000*1024;

#endif
