#include <array>
#include <cmath>
#include <vector>
#include <iostream>

#include "radix-sort.hpp"
#include "entries.hpp"

using std::vector;
using std::array;

vector<kmer_entry> radix_sort(vector<kmer_entry>& entries){
  vector<kmer_entry> temp(entries);

  int lb = 25;
  int rb = 13;

  for(;rb <= 64; lb += 13, rb += 13){
    counting_sort(entries, temp, lb, rb);
    entries.swap(temp);
  }
    
  return entries;
}

void counting_sort(vector<kmer_entry>& entries, vector<kmer_entry>& out, const int& lb, const int& rb){
  const int width = lb - rb + 1;

  const int n_buckets = std::pow(2, width);
  
  vector<int> count(omp_get_num_threads()*n_buckets);

  uint64_t mask = 1;
  for(int i = 0; i < width; ++i){
    mask *= 2;
  }
  mask -= 1;
  
  mask <<= (rb - 1);


  #pragma omp parallel for
  for(uint32_t i = 0; i < entries.size(); ++i){
    uint32_t bucket = (entries[i].int_kmer & mask) >> (rb - 1);
    ++count[bucket + n_buckets*omp_get_thread_num()];
  }

  for(int i = 1; i < omp_get_num_threads(); i++){
    for(int j = 0; j < n_buckets; j ++){
      count[j] += count[j + i*n_buckets];
    }
  }

  for(uint32_t i = 1; i < count.size(); i++){
    count[i] += count[i - 1];
  }

  for(uint32_t i = entries.size() - 1; i > 0; --i){
    uint32_t bucket = (entries[i].int_kmer & mask) >> (rb - 1);
    out[--count[bucket]] = entries[i];
  }
  uint32_t bucket = (entries[0].int_kmer & mask) >> (rb - 1);
  out[--count[bucket]] = entries[0];
}
