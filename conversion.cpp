#include "conversion.hpp"

uint64_t kmer_to_int(const std::string& kmer, const int& k){
  uint64_t int_val = 0;

  for(int i = k - 1; i >= 0; --i){
    uint64_t code;
    switch(kmer[i]){
    case 'A':
      code = 0; 
      break;
    case 'C':
      code = 1ULL << 62;
      break;
    case 'G':
      code = 2ULL << 62;
      break;
    default:
      code = 3ULL << 62;
      break;
    }
    int_val >>= 2;
    int_val |= code;
  }

  return int_val;
}

std::string int_to_kmer(uint64_t kmer, const int& k){
  std::string str_val(k, 'A');
  for(int i = 0; i < k; i++){
    uint64_t code = (3ULL << 62) & kmer;
    switch(code >> 62){
    case 0ULL:
      str_val[i] = 'A';
      break;
    case 1ULL:
      str_val[i] = 'C';
      break;
    case 2ULL:
      str_val[i] = 'G';
      break;
    case 3ULL:
      str_val[i] = 'T';
      break;
    }

    kmer <<= 2;
  }

  return str_val;
}

