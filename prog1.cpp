#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <chrono>

// run as:
// prog1 <input_file> <output_file> <k>
int main(int argc, char *argv[]){

  std::ifstream sequences;
  sequences.open(argv[1]);

  std::ofstream output;
  output.open(argv[2]);

  int k = atoi(argv[3]);
  
  std::string line;

  auto start = std::chrono::steady_clock::now();
  
  for(int i = 0; sequences.peek() != EOF; i++){
    if(i % k != 0){
      getline(sequences, line);
      getline(sequences, line);
      getline(sequences, line);
      getline(sequences, line);
    }
    else{
      getline(sequences, line);
      output << line << std::endl;
      getline(sequences, line);
      output << line << std::endl;
      getline(sequences, line);
      output << line << std::endl;
      getline(sequences, line);      
      output << line << std::endl;
    }
  }
  
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> dur = end - start;
  std::cout << "Total time " << dur.count() << " seconds\n.";
}
