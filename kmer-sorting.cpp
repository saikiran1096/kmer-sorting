#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdint>
#include <chrono>
#include <stdio.h>

#include "consts.hpp"
#include "entries.hpp"
#include "readwrite.hpp"

void in_memory_sort(const int& k, const std::string& in_file, const std::string& out_file){
  auto start = std::chrono::steady_clock::now();
  std::vector<kmer_entry> entries;
    
  if(in_file == "-"){
    entries = read_entries(std::cin, k);
  } else{
    std::ifstream in = std::ifstream(in_file);
    entries  =  read_entries(in, k);
    in.close();
  }

  std::chrono::duration<double> dur = std::chrono::steady_clock::now() - start;
  
  std::cout << "Reading and converting took " << dur.count() << " seconds\n";

  auto start_sort = std::chrono::steady_clock::now();
  // entries = radix_sort(entries);
  std::sort(entries.begin(), entries.end());
  dur = std::chrono::steady_clock::now() - start_sort;

  std::cout << "Sorting took " << dur.count() << " seconds\n";
  
  auto start_write = std::chrono::steady_clock::now();
  if(out_file == "-"){
    write_entries(std::cout, entries, k);
  } else {
    std::ofstream out(out_file);
    write_entries(out, entries, k);
    out.close();
  }
  auto end = std::chrono::steady_clock::now();
  
  dur = end - start_write;

  std::cout << "Writing and converting took " << dur.count() << " seconds\n";

  dur = end - start;
  std::cout << "Total time " << dur.count() << " seconds\n";  

}

// performs an n_pieces-way merge of the sorted pieces
void merge_and_write(const int& k, const uint32_t& n_pieces, std::ostream& out){ 
  std::vector<queue_entry> queue(n_pieces);
  std::vector<std::ifstream> files(n_pieces);

  for(size_t i = 0; i < queue.size(); i++){
    std::string piece_file = "/tmp/kmer-sorting" + std::to_string(i);
    files[i] = std::ifstream(piece_file);
    queue[i] = {read_compressed_entry(files[i], k), i};
  }

  std::make_heap(queue.begin(), queue.end());

  while(!queue.empty()){
    std::vector<kmer_entry> batch;
  
    while(!queue.empty() && batch.size() < WRITE_BATCH_SIZE){
      queue_entry entry = queue[0];
      batch.push_back(entry.k_entry);
      // std::cout << entry.k_entry.int_kmer << std::endl;
      size_t index = entry.index;
      
      std::pop_heap(queue.begin(), queue.end());
    
      if(files[index].peek() == EOF){
	queue.pop_back();
	files[index].close();
	std::string piece_file = "/tmp/kmer-sorting" + std::to_string(index);
	remove(piece_file.c_str());
      }
      
      else{
	queue.back().k_entry = read_compressed_entry(files[index], k);
	std::push_heap(queue.begin(), queue.end());
      }

    }
    write_entries(out, batch, k);
  }
}

void bounded_sort(const int& k, const std::string& in_file, const std::string& out_file, const uint32_t& memory){
  const uint32_t vec_size = memory / sizeof(kmer_entry);
  
  std::istream* in;
  std::ifstream in_reader;
  std::ostream* out;
  std::ofstream out_writer;
  
  if(in_file == "-"){
    in = &std::cin;
  } else{
    in_reader.open(in_file);
    in = &in_reader;
  }

  if(out_file == "-"){
    out = &std::cout;
  } else{
    out_writer.open(out_file);
    out = &out_writer;
  }

  
  uint32_t n_pieces = 0;

  auto start = std::chrono::steady_clock::now();

  
  while(in->peek() != EOF){
    std::vector<kmer_entry> entries = read_entries(*in, k, vec_size); 
    std::string piece_file = "/tmp/kmer-sorting" + std::to_string(n_pieces);
    std::ofstream out_piece(piece_file);

    std::sort(entries.begin(), entries.end());
    write_compressed_entries(out_piece, entries, k);
    
    ++n_pieces;
  }

  std::chrono::duration<double> dur = std::chrono::steady_clock::now() - start;

  
  std::cout << "Reading and sorting " << n_pieces << " pieces took " << dur.count() << " seconds."<< std::endl;
  
  auto merge_start = std::chrono::steady_clock::now();

  merge_and_write(k, n_pieces, *out);
  out_writer.close();
  
  dur = std::chrono::steady_clock::now() - merge_start;
  std::cout << "Writing and merging took " << dur.count() << " seconds." << std::endl;
  dur = std::chrono::steady_clock::now() - start;
  std::cout << "Total time tkaen " << dur.count() << std::endl;
}

// ./kmer-sorting <k> <in_file> <out_file> <buffer size in bytes>
int main(int argc, char** argv){
  int k = atoi(argv[1]);  
  std::string in_file = argv[2];
  std::string out_file = argv[3];
  uint32_t memory = atoi(argv[4]);

  if(memory == 0){
    in_memory_sort(k, in_file, out_file);
  }

  else{
    bounded_sort(k, in_file, out_file, memory);
  }
}
