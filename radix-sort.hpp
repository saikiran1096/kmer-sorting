#include <vector>
#include "entries.hpp"

std::vector<kmer_entry> radix_sort(std::vector<kmer_entry>& entries);
void counting_sort(std::vector<kmer_entry>& entries, std::vector<kmer_entry>& output, const int& lb, const int& rb);
